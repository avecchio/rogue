import sys
import csv

def main(args):
    if (len(args) == 2 and args[1].endswith('.csv')):
        with open(args[1]) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                print(row)
    else:
        print("Not enough arguments")
        print("Usage: python3 upload.py [file.csv]")

if __name__ == '__main__':
    main(sys.argv)
